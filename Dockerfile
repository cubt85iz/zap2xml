FROM perl:latest

RUN apt-get update && \
    apt-get -y upgrade

RUN apt-get install -y \
        libwww-perl \
        libhttp-cookies-perl \
        libjson-perl && \
    rm -rf /var/lib/apt/lists/*

COPY zap2xml.pl /zap2xml.pl

RUN mkdir /cache && mkdir /config
VOLUME /config
VOLUME /cache

ENTRYPOINT ["perl", "/zap2xml.pl", "-u $ZAP2XML_USER", "-p $ZAP2XML_PASSWORD", "-o /config/tvlistings.xmltv", "-c /cache/zap2it", "-F", "-O", "-T"]